# Mapfile Map Sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DITA-Compare-8_0_0_j/samples/sample-name`.*

---

## Summary

This sample compares two DITA map files in1.ditamap and in2.ditamap, it does not compare the referenced topics. Because of this, the topic files are not included in the sample. Please read the content of the DITA markup result (results/dita-markup.ditamap) as it is designed to illustrate several of our DITA comparison product's features. For more details see: [Mapfile Map documentation](https://docs.deltaxml.com/dita-compare/latest/mapfile-map-sample-8290419.html).

## Running the sample via the Ant build script

The sample comparison can be run via an Apache Ant build script using the following commands.

| Command | Actions Performed |
| --- | --- |
| ant run | Run all four comparisons. |
| ant run-dita-markup | Run the DITA Markup comparison. |
| ant run-oxygen-tcs | Run the oXygen tracked changes comparison. |
| ant clean | Remove the generate output. |

If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line

The sample can be run directly by issuing the following command

    java -jar ../../deltaxml-dita.jar compare mapfile in1.ditamap in2.ditamap out.ditamap output-format=dita-markup

Where `dita-markup` format enumeration can be replaced by one of the tracked changes format enumerations.

To clean up the sample directory, run the following Ant command.

	ant clean